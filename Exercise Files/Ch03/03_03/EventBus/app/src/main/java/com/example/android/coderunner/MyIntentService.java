package com.example.android.coderunner;

import android.app.IntentService;
import android.content.Intent;

import com.example.android.coderunner.events.MessageEvents;

import de.greenrobot.event.EventBus;

public class MyIntentService extends IntentService {

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        MessageEvents event = new MessageEvents("from the service");
        EventBus.getDefault()       //returns the instance of the EventBus class
                .post(event);       //posts the event of type MessageEvents to the message queue of the eventbus.

        try
        {
            Thread.sleep(2000);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        event.setMessage("another message from the service");
        EventBus.getDefault()       //returns the instance of the EventBus class
                .post(event);       //posts the event of type MessageEvents to the message queue of the eventbus.
    }
}
