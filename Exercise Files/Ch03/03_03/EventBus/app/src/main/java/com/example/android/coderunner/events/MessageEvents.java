package com.example.android.coderunner.events;

public class MessageEvents
{
    private String message;

    public MessageEvents(String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
